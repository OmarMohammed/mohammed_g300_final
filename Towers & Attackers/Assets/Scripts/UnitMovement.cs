﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Unit))]
public class UnitMovement : MonoBehaviour
{
    private Transform target;
    [HideInInspector]
    public int wavepointIndex;

    private Unit unit;

    private void Start()
    {
        wavepointIndex = Waypoints.points.Length - 1;
        unit = GetComponent<Unit>();
        target = Waypoints.points[wavepointIndex];
    }

    private void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * unit.speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= .4f)
        {
            GetNextWaypoint();
        }

        unit.speed = unit.startSpeed;
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex <= 0)
        {
            EndPath();
            return;
        }
        wavepointIndex--;
        target = Waypoints.points[wavepointIndex];
    }

    void EndPath()
    {
        unit.isAtEnd = true;
    }
}
