﻿using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float startSpeed = 10f;
    private Transform target;
    private Turret targetTurret;

    [HideInInspector]
    public float speed;

    public float StartHealth = 100;
    private float health;

    public int moneyGain = 50;

    public GameObject deathEffect;

    [Header("Unity Stuff")]
    public Image healthBar;

    private bool isDead = false;
    public string playerTurret = "PlayerTurret";
    public string playerUnit = "PlayerUnit";
    public string playerBase = "PlayerBase";

    public GameObject bulletPrefab;
    public Transform firePoint;

    public float turnSpeed = 10f;

    [Header("General Attack")]
    public float range = 8f;
    public bool suicides = false;
    public bool isAtEnd = false;

    [Header("Bullets")]
    public float fireRate = 1f;
    private float firecountdown = 0f;
    private bool fireing = false;

    private void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, .1f);
        speed = startSpeed;
        health = StartHealth;
    }

    private void Update()
    {
        if (target == null)
        {
            speed = startSpeed;
            return;
        }
        LockOnTarget();
        speed = 0;
        fireing = true;
        if (firecountdown <= 0f)
        {
            Shoot();
            firecountdown = 1f / fireRate;
        }
        firecountdown -= Time.deltaTime;
    }
    public void TakeDamage (float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / StartHealth;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    public void Slow (float pct)
    {
        if (fireing)
        {
            return;
        }
        speed = startSpeed * (1f - pct);
    }

    void Die()
    {
        isDead = true;
        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);

        PlayerStats.Money += moneyGain;
        wavespawner.EnemiesAlive--;
        Destroy(gameObject);
    }

    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void Shoot()
    {
        if (suicides)
        {
            GameObject[] turrets = GameObject.FindGameObjectsWithTag(playerTurret);
            GameObject[] units = GameObject.FindGameObjectsWithTag(playerUnit);
            GameObject[] playerBases = GameObject.FindGameObjectsWithTag("PlayerBase");

            foreach (GameObject turret in turrets)
            {
                float distanceToTurret = Vector3.Distance(transform.position, turret.transform.position);
                if (distanceToTurret < range)
                {
                    turret.GetComponent<Turret>().TakeDamage(100);
                }
            }

            foreach (GameObject unit in units)
            {
                float distanceToTurret = Vector3.Distance(transform.position, unit.transform.position);
                if (distanceToTurret < range)
                {
                    unit.GetComponent<Unit>().TakeDamage(100);
                }
            }
            foreach (GameObject playerBase in playerBases)
            {
                float distanceToTurret = Vector3.Distance(transform.position, playerBase.transform.position);
                if (distanceToTurret < range)
                {
                    PlayerStats.Health -= 175;
                }
            }

            Die();
            return;
        }
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        EnemyBullet bullet = bulletGO.GetComponent<EnemyBullet>();

        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }

    void UpdateTarget()
    {
        if(isAtEnd)
        {
            GameObject playerbase = GameObject.FindGameObjectWithTag(playerBase);
            target = playerbase.transform;
            return;
        }
        GameObject[] turrets = GameObject.FindGameObjectsWithTag(playerTurret);
        GameObject[] units = GameObject.FindGameObjectsWithTag(playerUnit);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestPlayer = null;

        foreach (GameObject turret in turrets)
        {
            float distanceToTurret = Vector3.Distance(transform.position, turret.transform.position);
            if (distanceToTurret < shortestDistance)
            {
                shortestDistance = distanceToTurret;
                nearestPlayer = turret;
            }
        }

        foreach (GameObject unit in units)
        {
            float distanceToTurret = Vector3.Distance(transform.position, unit.transform.position);
            if (distanceToTurret < shortestDistance)
            {
                shortestDistance = distanceToTurret;
                nearestPlayer = unit;
            }
        }
        if (nearestPlayer != null && shortestDistance <= range)
        {
            target = nearestPlayer.transform;
            targetTurret = nearestPlayer.GetComponent<Turret>();
        }
        else
        {
            target = null;
            fireing = false;
        }
    }
}
