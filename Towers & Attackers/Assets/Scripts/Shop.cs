﻿using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretBlueprint standardTurret;
    public TurretBlueprint missileTurret;
    public TurretBlueprint laserBeamer;
    public TurretBlueprint turretWithPanels;
    public GameObject simpleUnit;
    public int simpleUnitPrice = 100;
    public GameObject toughUnit;
    public int toughUnitPrice = 150;
    public GameObject fastUnit;
    public int fastUnitPrice = 75;
    public GameObject suicideUnit;
    public int suicideUnitPrice = 175;
   
    public Transform playerBase;
    BuildManager buildManager;

    private void Start()
    {
        buildManager = BuildManager.instance;
        buildManager.SelectTurretToBuild(standardTurret);
    }
    public void SelectStandardTurret ()
    {
        buildManager.SelectTurretToBuild(standardTurret);
    }

    public void SelectMissileTurret()
    {
        buildManager.SelectTurretToBuild(missileTurret);
    }

    public void SelectLaserBeamer()
    {
        buildManager.SelectTurretToBuild(laserBeamer);
    }

    public void SelectTurretWithPanels()
    {
        buildManager.SelectTurretToBuild(turretWithPanels);
    }

    public void SelectsimpleUnit()
    {
        if (PlayerStats.Money >= simpleUnitPrice)
        {
            Instantiate(simpleUnit, playerBase.transform.position, playerBase.transform.rotation);
            PlayerStats.Money -= simpleUnitPrice;
        }
    }

    public void SelectToughUnit()
    {
        if (PlayerStats.Money >= toughUnitPrice)
        {
            Instantiate(toughUnit, playerBase.transform.position, playerBase.transform.rotation);
            PlayerStats.Money -= toughUnitPrice;
        }
    }
    public void SelectFastUnit()
    {
        if (PlayerStats.Money >= fastUnitPrice)
        {
            Instantiate(fastUnit, playerBase.transform.position, playerBase.transform.rotation);
            PlayerStats.Money -= fastUnitPrice;
        }
    }

    public void SelectSuicideUnit()
    {
        if (PlayerStats.Money >= suicideUnitPrice)
        {
            Instantiate(suicideUnit, playerBase.transform.position, playerBase.transform.rotation);
            PlayerStats.Money -= suicideUnitPrice;
        }
    }
}
