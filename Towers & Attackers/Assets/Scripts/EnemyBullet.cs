﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    private Transform target;

    public float speed = 70f;

    public int damage = 10;

    public float explosionRadius = 0f;
    public GameObject impactEffect;

    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        Vector3 dir = target.position - transform.position;
        float distanceThisFram = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFram)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFram, Space.World);
        transform.LookAt(target);
    }

    void HitTarget()
    {
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 5f);

        if (explosionRadius > 0f)
        {
            Explode();
        }
        else
        {
            Damage(target);
        }

        Destroy(gameObject);
    }

    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "PlayerTurret")
            {
                Damage(collider.transform); 
            }
            if (collider.tag == "PlayerUnit")
            {
                Damage(collider.transform);
            }
            if (collider.tag == "PlayerBase")
            {
                PlayerStats.Health -= damage;
            }
        }
    }

    void Damage(Transform player)
    {
        if (player.tag == "PlayerBase")
        {
            PlayerStats.Health -= damage;
        }
        if (player.tag == "PlayerTurret")
        {
            Turret pt = player.GetComponent<Turret>();
            if (player != null)
            {
                pt.TakeDamage(damage);
            }
        }
        if (player.tag == "PlayerUnit")
        {
            Unit pu = player.GetComponent<Unit>();
            if (player != null)
            {
                pu.TakeDamage(damage);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
