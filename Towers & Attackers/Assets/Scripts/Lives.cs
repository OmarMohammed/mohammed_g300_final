﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lives : MonoBehaviour
{
    public Text livesText;
    public Text enemyBaseText;

    void Update()
    {
        livesText.text = "PlayerBase: " + PlayerStats.Health.ToString() + "Health";
        enemyBaseText.text = "EnemyBase: " + PlayerStats.EnemyHealth.ToString() + "Health";
    }
}
