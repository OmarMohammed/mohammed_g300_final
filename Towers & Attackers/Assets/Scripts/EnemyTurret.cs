﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyTurret : MonoBehaviour
{
    private Transform target;
    private Unit targetUnit;

    [Header("General")]

    public float range = 15f;
    public float startHealth = 750;
    private float health;

    [Header("Use Bullets (default)")]
    public float fireRate = 1f;
    private float fireCountdown = 0f;

    [Header("Use Laser")]
    public bool useLaser = false;

    public int damageOverTime = 30;
    public float slowAmount = .5f;

    public LineRenderer lineRenderer;
    public ParticleSystem impactEffect;
    public Light impactLight;

    [Header("Unity Setup Fields")]

    public Transform partToRotate;
    public float turnSpeed = 10f;
    public GameObject playerTurret;

    [HideInInspector]
    public GameObject node;

    public GameObject deathEffect;
    public Image healthBar;

    public string unitTag = "PlayerUnit";

    public GameObject bulletPrefab;
    public Transform firePoint;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, .5f);
        health = startHealth;
    }

    void UpdateTarget()
    {
        GameObject[] units = GameObject.FindGameObjectsWithTag(unitTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestunit = null;

        foreach (GameObject unit in units)
        {
            float distanceToUnit = Vector3.Distance(transform.position, unit.transform.position);
            if (distanceToUnit < shortestDistance)
            {
                shortestDistance = distanceToUnit;
                nearestunit = unit;
            }
        }

        if (nearestunit != null && shortestDistance <= range)
        {
            target = nearestunit.transform;
            targetUnit = nearestunit.GetComponent<Unit>();
        }
        else
        {
            target = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            if (useLaser)
            {
                if (lineRenderer.enabled)
                {
                    lineRenderer.enabled = false;
                    impactEffect.Stop();
                    impactLight.enabled = false;
                }
            }
            return;
        }

        LockOnTarget();

        if (useLaser)
        {
            Laser();
        }
        else
        {
            if (fireCountdown <= 0f)
            {
                Shoot();
                fireCountdown = 1f / fireRate;
            }
            fireCountdown -= Time.deltaTime;
        }
    }

    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void Laser()
    {
        targetUnit.TakeDamage(damageOverTime * Time.deltaTime);
        targetUnit.Slow(slowAmount);

        if (!lineRenderer.enabled)
        {
            lineRenderer.enabled = true;
            impactEffect.Play();
            impactLight.enabled = true;
        }
        lineRenderer.SetPosition(0, firePoint.position);
        lineRenderer.SetPosition(1, target.position);

        Vector3 dir = firePoint.position - target.position;

        impactEffect.transform.position = target.position + dir.normalized;

        impactEffect.transform.rotation = Quaternion.LookRotation(dir);

    }

    void Shoot()
    {
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        EnemyBullet bullet = bulletGO.GetComponent<EnemyBullet>();
        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        healthBar.fillAmount = health / startHealth;
        if (health <= 0)
        {
            Die();
        }

    }
    void Die()
    {
        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        GameObject playerTurret_ = (GameObject)Instantiate(playerTurret, transform.position, transform.rotation);
        node.GetComponent<Node>().turret = playerTurret_;
        node.GetComponent<Node>().isEnemyTurret = false;
        node.GetComponent<Node>().currentTurret = playerTurret_;
        playerTurret_.GetComponent<Turret>().node = node;
        Destroy(gameObject);
    }
}
