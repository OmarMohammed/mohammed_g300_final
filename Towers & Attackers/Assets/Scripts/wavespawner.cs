﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class wavespawner : MonoBehaviour
{
    public static int EnemiesAlive = 0;

    public Wave[] waves;

    public Transform enemySpawnPoint;

    public float timeBetweenWaves = 2f;
    private float countdown = 2f;
    public int numberOfWaves;
    public GameObject standardEnemy;
    public GameObject fastEnemy;
    public GameObject toughEnemy;
    public GameObject suicideEnemy;
    private int enemiesToSpawn = 0;

    public Text waveCountDownText;

    public GameManager gameManager;

    private int waveIndex = 0;

    private void Update()
    {
        if (EnemiesAlive > 0)
        {
            return;
        }
        if(countdown <= 0)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
            return;
        }

        countdown -= Time.deltaTime;

        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

        waveCountDownText.text = string.Format("{0:00.00}", countdown);
    }

    IEnumerator SpawnWave()
    {
        PlayerStats.Waves++;

        if (waveIndex >= numberOfWaves)
        {
            enemiesToSpawn = waveIndex * 4;
            for (int i = 0; i < enemiesToSpawn; i++)
            {
                SpawnEnemy(standardEnemy);
                yield return new WaitForSeconds(waveIndex/5);
                EnemiesAlive += 1;
            }
            for (int i = 0; i < enemiesToSpawn; i++)
            {
                SpawnEnemy(fastEnemy);
                yield return new WaitForSeconds(waveIndex/5);
                EnemiesAlive += 1;
            }
            for (int i = 0; i < enemiesToSpawn; i++)
            {
                SpawnEnemy(toughEnemy);
                yield return new WaitForSeconds(waveIndex/5);
                EnemiesAlive += 1;
            }
            for (int i = 0; i < enemiesToSpawn; i++)
            {
                SpawnEnemy(suicideEnemy);
                yield return new WaitForSeconds(waveIndex/5);
                EnemiesAlive += 1;
            }
        }

        if (waveIndex < numberOfWaves)
        {
            Wave wave = waves[waveIndex];

            EnemiesAlive = wave.count;

            for (int i = 0; i < wave.count; i++)
            {
                SpawnEnemy(wave.enemy);
                yield return new WaitForSeconds(1f / wave.rate);
            }
        }
        waveIndex++;
    }

    void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, enemySpawnPoint.position, enemySpawnPoint.rotation);
    }
}
