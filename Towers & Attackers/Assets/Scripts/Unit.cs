﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Unit : MonoBehaviour
{
    public float startSpeed = 10f;
    private Transform target;
    private EnemyTurret targetEnemy;

    [HideInInspector]
    public float speed;

    public float StartHealth = 100;
    private float health;

    public GameObject deathEffect;

    [Header("Unity Stuff")]
    public Image healthBar;

    private bool isDead = false;
    public string enemyTurret = "EnemyTurret";
    public string enemy = "Enemy";
    public string enemyBase = "EnemyBase";

    public GameObject bulletPrefab;
    public Transform firePoint;

    public float turnSpeed = 10f;

    [Header("General Attack")]
    public float range = 8f;
    public bool suicides = false;
    public bool isAtEnd = false;

    [Header("Bullets")]
    public float fireRate = 1f;
    private float firecountdown = 0f;
    private bool fireing = false;

    private void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, .1f);
        speed = startSpeed;
        health = StartHealth;
    }

    private void Update()
    {
        if (target == null)
        {
            speed = startSpeed;
            return;
        }
        LockOnTarget();
        speed = 0;
        fireing = true;
        if (firecountdown <= 0f)
        {
            Shoot();
            firecountdown = 1f / fireRate;
        }
        firecountdown -= Time.deltaTime;
    }
    public void TakeDamage(float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / StartHealth;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    public void Slow(float pct)
    {
        if (fireing)
        {
            return;
        }
        speed = startSpeed * (1f - pct);
    }

    void Die()
    {
        isDead = true;
        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        Destroy(gameObject);
    }

    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void Shoot()
    {
        if (suicides)
        {
            GameObject[] turrets = GameObject.FindGameObjectsWithTag(enemyTurret);
            GameObject[] units = GameObject.FindGameObjectsWithTag(enemy);
            GameObject[] enemyBases = GameObject.FindGameObjectsWithTag("EnemyBase");

            foreach (GameObject turret in turrets)
            {
                float distanceToTurret = Vector3.Distance(transform.position, turret.transform.position);
                if (distanceToTurret < range)
                {
                    turret.GetComponent<EnemyTurret>().TakeDamage(175);
                }
            }

            foreach (GameObject unit in units)
            {
                float distanceToTurret = Vector3.Distance(transform.position, unit.transform.position);
                if (distanceToTurret < range)
                {
                    unit.GetComponent<Enemy>().TakeDamage(175);
                }
            }
            foreach (GameObject enemyBase in enemyBases)
            {
                float distanceToTurret = Vector3.Distance(transform.position, enemyBase.transform.position);
                if (distanceToTurret < range)
                {
                    PlayerStats.EnemyHealth -= 175;
                }
            }
            Die();
            return;
        }
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();

        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }

    void UpdateTarget()
    {
        if (isAtEnd)
        {
            GameObject playerbase = GameObject.FindGameObjectWithTag(enemyBase);
            target = playerbase.transform;
            return;
        }
        GameObject[] enemyturrets = GameObject.FindGameObjectsWithTag(enemyTurret);
        GameObject[] enemyunits = GameObject.FindGameObjectsWithTag(enemy);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemyunits)
        {
            float distanceToTurret = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToTurret < shortestDistance)
            {
                shortestDistance = distanceToTurret;
                nearestEnemy = enemy;
            }
        }
        
        foreach (GameObject enemy in enemyturrets)
        {
            float distanceToTurret = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToTurret < shortestDistance)
            {
                shortestDistance = distanceToTurret;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<EnemyTurret>();
        }
        else
        {
            target = null;
            fireing = false;
        }
    }
}
