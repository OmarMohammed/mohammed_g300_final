﻿using UnityEngine;
using UnityEngine.UI;

public class NodeUI : MonoBehaviour
{
    public GameObject ui;

    public Text upgradeCost;
    public Button upgradeButton;
    public Image healthBar;

    public Text sellAmount;

    private Node target;

    private void Update()
    {
        if(target == null)
        {
            return;
        }
        if(target.currentTurret == null)
        {
            return;
        }
        healthBar.fillAmount = target.currentTurret.GetComponent<Turret>().health / target.currentTurret.GetComponent<Turret>().startHealth;
    }

    public void SetTarget(Node _target)
    {
        target = _target;

        if(target.isEnemyTurret)
        {
            return;
        }

        transform.position = target.GetBuildPosition();

        if (!target.isUpgraded)
        {
            upgradeCost.text = "$" + target.turretBlueprint.upgradeCost;
            upgradeButton.interactable = true;
        } else
        {
            upgradeCost.text = "Done";
            upgradeButton.interactable = false;
        }

        sellAmount.text = "$" + target.turretBlueprint.GetSellAmount();

        ui.SetActive(true);
    }

    public void Hide()
    {
        ui.SetActive(false);
    }

    public void Upgrade()
    {
        target.UpgradeTurret();
        BuildManager.instance.DeselectNode();
    }

    public void Sell()
    {
        target.SellTurret();
        BuildManager.instance.DeselectNode();
    }
}
