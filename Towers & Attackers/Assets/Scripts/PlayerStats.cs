﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{

    public static int Money;
    public int startMoney = 400;

    public static int Health;
    public int startHealth = 10000;

    public static int EnemyHealth;
    public int startEnemyHealth = 10000;

    public static int Waves;
    private void Start()
    {
        Money = startMoney;
        Health = startHealth;
        EnemyHealth = startEnemyHealth;

        Waves = 0;
    }
}
