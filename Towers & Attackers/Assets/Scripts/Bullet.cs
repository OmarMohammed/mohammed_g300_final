﻿using UnityEngine;

public class Bullet : MonoBehaviour
{

    private Transform target;

    public float speed = 70f;

    public int damage = 50;

    public float explosionRadius = 0f;
    public GameObject impactEffect;

    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFram = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFram)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFram, Space.World);
        transform.LookAt(target);
    }

    void HitTarget()
    {
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 5f);

        if (explosionRadius > 0f)
        {
            Explode();
        } else
        {
            Damage(target);
        }

        Destroy(gameObject);
    }

    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "EnemyTurret")
            {
                Damage(collider.transform);
            }
            if (collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
            if (collider.tag == "EnemyBase")
            {
                PlayerStats.EnemyHealth -= damage;
            }
        }
    }
     
    void Damage (Transform enemy)
    {
        if (enemy.tag == "EnemyBase")
        {
            PlayerStats.EnemyHealth -= damage;
        }
        if (enemy.tag == "EnemyTurret")
        {
            EnemyTurret et = enemy.GetComponent<EnemyTurret>();
            if (enemy != null)
            {
                et.TakeDamage(damage);
            }
        }
        if (enemy.tag == "Enemy")
        {
            Enemy e = enemy.GetComponent<Enemy>();
            if (enemy != null)
            {
                e.TakeDamage(damage);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
